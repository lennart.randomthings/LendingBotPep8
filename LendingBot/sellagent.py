import CLASS.DATABASE
import CLASS.POLONIEXAPI
import helper.telegramsend
import logging
import helper.tradefunctions
import helper.telegramsend
import numpy as np
import re
import time

# Init Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Sellagent.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Init Config
conf = helper.config.initconfig()

# Init Database
Database = CLASS.DATABASE.DATABASE()
Database.connect()
Database.setup()

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()
Poloniexapi.init()

while True:
    Trades = Database.requestTrade()
    for Trade in Trades:
        CurrencyPair = Trade['CurrencyPair']
        amount = float(Trade['AmountInCurrency'])
        print(Trade['CurrencyPair'])
        print(Poloniexapi.returnTickerhighest(CurrencyPair))
        print(Trade['takeprofit_price'])
        
        if Poloniexapi.returnTickerhighest(CurrencyPair) > Trade['takeprofit_price'] :
            profit = 0
            amount_sold = 0
            totals = 0
            try:
                recap = helper.tradefunctions.sell_asset(CurrencyPair, amount, store = True) # Sell if we reached the takeprofit
                profit = float(recap['totals']) - float(Trade['Total'])
                amount_sold = float(np.sum(recap['amount_sold']))
                totals = float(np.sum(recap['totals']))
                logging.info("Sell: " + str(recap))
            except:
                logging.ERROR("Except: Sell: " + str(recap))
                pass
            status = "We sold {} {} to {} {}\n Profit is: {}".format(amount_sold,re.split('_', CurrencyPair)[1],totals,re.split('_', CurrencyPair)[0],profit)
            helper.telegramsend.send(status)
            logging.info(status)
            Database.sellTrade(Trade)
        time.sleep(1)
