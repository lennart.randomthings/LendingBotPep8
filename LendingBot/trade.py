# Args [CurrencyPair] [Amount] [Period] [Index]
# python3 LendingBot/trade.py "TRX_NFT" 105 300 0
import sys
import helper.tradefunctions
from poloniex import Poloniex
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from time import time, sleep
import re
import helper.tradingfunctions
import CLASS.POLONIEXAPI
import logging
import helper.telegramsend
import helper.sftp
import CLASS.DATABASE
import helper.config
import random
import keras.models
import pickle as pk
import json
import os

# Init Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Trading.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Init Config
conf = helper.config.initconfig()

# Init Database
Database = CLASS.DATABASE.DATABASE()
Database.connect()
Database.setup()

# Set CurrencyPair
CurrencyPair = 'BTC_ETH'
CurrencyPair = sys.argv[1]

# Init PoloniexApi
Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()
Poloniexapi.init()

amount_dollar = 0.001 # amount in dollar that we invest for each trade
amount_dollar = float(sys.argv[2])
if len(sys.argv) > 3:
    Period = int(sys.argv[3])
else:
    Period = 300
if len(sys.argv) >4:
    index = int(sys.argv[4])
else:
    index = 0
buy_signal = 0
sell_signal = 0

# Init TraderStatus
TraderStatus = {}
TraderStatus['CurrencyPair'] = CurrencyPair
TraderStatus['stoploss_price'] = 0.0
TraderStatus['takeprofit_price'] = 0.0
TraderStatus['Proba1'] = 0.0
TraderStatus['Period'] = Period
TraderStatus['index'] = index

if not os.path.exists('./Models'):
        os.makedirs('./Models')

# Initial Wait
status = "Wait for Start"
print(status)
logging.info(status)
TraderStatus['Status'] = status
Database.insertTrader(TraderStatus)
sleep(random.randrange(0,60))
sleep(Period*index)
sleep(120)

######################### 0 - Load necessary data, define constants
# (0) Define constants used for the trading loop
status = "Start with: Currency: " + CurrencyPair + " Amount: " + str(amount_dollar)
print(status)
logging.info(status)
TraderStatus['Status'] = status
Database.insertTrader(TraderStatus)

model = Database.requestBest(CurrencyPair)

polo = Poloniex(Poloniexapi.key, Poloniexapi.secret) #TODO über die Klasse abwickeln

# (iii) According to your strategy (see article III), define a min and a max thresholds for following it (by default : 0.5 and 1)
min_threshold, max_threshold = 0.5, 1 #TODO use from Database

######################### II - One trade at a time trading bot
print('\n---------------- \n---------------- \n We start to trade {} with nominal amount {} {}, it is {}'.format(CurrencyPair ,amount_dollar, re.split('_', CurrencyPair)[0], datetime.now()))
#try:  #TODO insert try after testing
while True:
    while len(model)<=index:
        # Wait for a ROI
        status = "Wait for a ROI 120s"
        print(status, end="\r")
        logging.info(status)
        TraderStatus['Status'] = status
        Database.insertTrader(TraderStatus)
        sleep(120)
        model = Database.requestBest(CurrencyPair)

    if int(time())%Period < 10 :
        model = Database.requestBest(CurrencyPair)
        takeprofit = model[index]['takeprofit']
        stoploss = model[index]['stoploss']
        min_threshold = model[index]['min']
        nPCs = model[index]['nPCs']
        TraderStatus['takeprofit'] = takeprofit
        TraderStatus['stoploss'] = stoploss
        TraderStatus['min_threshold'] = min_threshold
        TraderStatus['nPCs'] = nPCs
        

        # (i) Load scalers and PCA
        # scale_fct = pk.loads(jsonpickle.decode(model[index]['pickle']['scale_fct']))
        # pca = pk.loads(jsonpickle.decode(model[index]['pickle']['pca']))
        # pca_scaler = pk.loads(jsonpickle.decode(model[index]['pickle']['pca_scaler']))
        helper.sftp.readfile('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair,Period),CurrencyPair,Period)
        with open('./Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_scaler.pkl'.format(CurrencyPair, Period, stoploss, takeprofit), 'rb') as f:
            scale_fct = pk.load(f)
        with open('./Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_pca.pkl'.format(CurrencyPair, Period, stoploss, takeprofit), 'rb') as f:
            pca = pk.load(f)
        with open('./Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_pca_scaler.pkl'.format(CurrencyPair, Period, stoploss, takeprofit), 'rb') as f:
            pca_scaler = pk.load(f)

        # (ii) Pick the best model
        #TODO pick up the best Stoploss and Takeprofit
        #recap = pd.read_csv('./Results/CurrencyPair{}/Period{}/Comparative_All_models_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period,stoploss, takeprofit)).sort_values('Accuracy', ascending = False)
        status = 'Best model is with {} PCs, stoploss: {}, takeprofit: {} ROI: {} Threshold: {}'.format(nPCs,stoploss,takeprofit,model[index]['ROI'],model[index]['min'])
        print(status)
        logging.info(status)
        TraderStatus['Status'] = status
        Database.insertTrader(TraderStatus)

        # Load Model from Database
        # clf = keras.models.model_from_json(model[index]['Model'])
        helper.sftp.readmodels('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit),CurrencyPair,Period,nPCs,stoploss,takeprofit)
        clf = keras.models.load_model('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
    
        if Poloniexapi.returnBalancesavailable(re.split('_', CurrencyPair)[0]) > amount_dollar:
            print("")
            print("It is {} ... Let's trade {} !".format(datetime.now(), CurrencyPair))
            # I- Request the data
            raw = polo.returnChartData(CurrencyPair, period = Period, start = int(time()) - Period*10000)
            df = pd.DataFrame(raw).iloc[1:] # First row may contain useless data
            df['date'] = pd.to_datetime(df["date"], unit='s')
            while (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period : #Check we've got the very recent candle : we stay here until Poloniex delivers it
                status = '{} : Waiting for actualized data... time: {}'.format(datetime.now(),(datetime.utcnow() - df['date'].iloc[-1]).total_seconds())
                logging.info(status)
                TraderStatus['Status'] = status
                Database.insertTrader(TraderStatus)
                print(status,end="\r")
                sleep(random.randrange(1,10))
                raw = polo.returnChartData(CurrencyPair, period = Period, start = int(time()) - Period*10000)
                df = pd.DataFrame(raw).iloc[1:] # First row may contain useless data
                df['date'] = pd.to_datetime(df["date"], unit='s')
                if (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period*10:
                    break
            if (datetime.utcnow() - df['date'].iloc[-1]).total_seconds() > Period*10:
                continue
            print("")
            print('We collected the recent data at {}'.format(datetime.now()))
            df = df[['close', 'date', 'high', 'low', 'open', 'volume']] # only keep required data

            # II - Compute predictions
            df = helper.tradingfunctions.compute_variables1(df)
            df_final = pd.DataFrame(pca_scaler.transform(pca.transform(scale_fct.transform(df.drop('date', 1)))))
            df_final = df_final.iloc[:, :nPCs]
            df['preds'] = (clf.predict(df_final.iloc[:, :nPCs]) > 0.5)*1
            df['proba1'] = clf.predict(df_final.iloc[:, :nPCs])

            # III - If criterion reached, we buy the asset and track the trade until we reach either the takeprofit or the stoploss
            status = 'Proba1 is {}, stoploss: {}, takeprofit: {}'.format(df['proba1'].iloc[-1],stoploss,takeprofit)
            print(status)
            logging.info(status)
            TraderStatus['Status'] = status
            TraderStatus['stoploss_price'] = 0.0
            TraderStatus['takeprofit_price'] = 0.0
            TraderStatus['Proba1'] = df['proba1'].iloc[-1]
            Database.insertTrader(TraderStatus)
            buy_signal = ((df['proba1'].iloc[-1] > min_threshold) & (df['proba1'].iloc[-1] < max_threshold))*1
            if buy_signal:
                recap_trade1 = helper.tradefunctions.buy_asset(pair = CurrencyPair, amount = amount_dollar, store = True)
                data = json.loads(recap_trade1.to_json())
                price = np.mean(recap_trade1['Rate'])
                amount = float(np.sum(recap_trade1['AmountInCurrency']))
                stoploss_price = price*(1-stoploss)
                takeprofit_price = price*(1+takeprofit)
                status = 'We buy {} {} with {} with a rate of {} ! \n stoploss: {}\n takeprofit: {}'.format(amount,re.split('_', CurrencyPair)[1],re.split('_', CurrencyPair)[0],price,stoploss_price,takeprofit_price)
                data['stoploss_price'] = stoploss_price
                data['takeprofit_price'] = takeprofit_price
                data['CurrencyPair'] = CurrencyPair
                data['Type'] = 'buy'
                data['Total'] = np.sum(recap_trade1['Total$'])
                data['Rate'] = np.sum(recap_trade1['Rate'])
                data['AmountInCurrency'] = np.sum(recap_trade1['AmountInCurrency'])
                Database.insertTrade(data)
                print(status)
                logging.info(status)
                helper.telegramsend.send(status)
                TraderStatus['Status'] = status
                TraderStatus['stoploss_price'] = stoploss_price
                TraderStatus['takeprofit_price'] = takeprofit_price
                Database.insertTrader(TraderStatus)
                print("wait for restart: " + str(Period*100) + "s")
                sleep(Period*100)
                # recap_trade2 = helper.tradefunctions.track_investment(CurrencyPair, price, amount, stoploss_price, takeprofit_price)
                # status = 'We made a profit of {} {} with trade in {}!'.format(np.sum(recap_trade1['Total$']) - np.sum(recap_trade2['Total$Adjusted']),re.split('_', CurrencyPair)[1],CurrencyPair)
                # print(status)
                # logging.info(status)
                # helper.telegramsend.send(status)
                # TraderStatus['Status'] = status
                # TraderStatus['stoploss_price'] = 0.0
                # TraderStatus['takeprofit_price'] = 0.0
                # Database.insertTrader(TraderStatus)
        else:
            status = "Not enought Currency"
            print(status)
            logging.info(status)
            TraderStatus['Status'] = status
            Database.insertTrader(TraderStatus)
    print("It is {} ... We wait for the time : {} seconds to go. ".format(datetime.now(), Period - int(time())%Period), end="\r")
    sleep(5)
# except TypeError:
#     #helper.telegramsend.send("TypeError CRASHED!!!!")
#     print("TypeError CRASHED!!!!")
# except AttributeError:
#     #helper.telegramsend.send("AttributeError CRASHED!!!!")
#     print("AttributeError CRASHED!!!!")
# except Exception as e:
#     #helper.telegramsend.send("Error '{0}' occurred. Arguments {1}.".format(e, e.args))
#     print("Error '{0}' occurred. Arguments {1}.".format(e, e.args))


# except:
#     # If the loop encountered an error, just sell everything
#     print('We encountered an error, we sell everything')
#     currency = re.split('_', CurrencyPair)[1]
#     #helper.tradefunctions.sell_everything(currency)