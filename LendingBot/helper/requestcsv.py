import requests
import time
import csv
import os
import multiprocessing as mp


def request(CurrencyPair,Period):
    # Init the time
    now = time.time()
    _1month = 2678400 # seconds in 1 month
    now_1month = now - _1month # UNIX time 1 month before

    # Create all folders
    if not os.path.exists('./Data/CurrencyPair{}'.format(CurrencyPair)):
        os.makedirs('./Data/CurrencyPair{}'.format(CurrencyPair))
    if not os.path.exists('./Data/CurrencyPair{}/Period{}'.format(CurrencyPair,Period)):
        os.makedirs('./Data/CurrencyPair{}/Period{}'.format(CurrencyPair,Period))
    if not os.path.exists('./Models/CurrencyPair{}'.format(CurrencyPair)):
        os.makedirs('./Models/CurrencyPair{}'.format(CurrencyPair))
    if not os.path.exists('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair,Period)):
        os.makedirs('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair,Period))
    if not os.path.exists('./Results/CurrencyPair{}'.format(CurrencyPair)):
        os.makedirs('./Results/CurrencyPair{}'.format(CurrencyPair))
    if not os.path.exists('./Results/CurrencyPair{}/Period{}'.format(CurrencyPair,Period)):
        os.makedirs('./Results/CurrencyPair{}/Period{}'.format(CurrencyPair,Period))

    # Request loop for 72 months
    for x in range(72):
        url = "https://poloniex.com/public?command=returnChartData&currencyPair="+ CurrencyPair + "&start=" + str(now_1month) + "&end=" + str(now) + "&period=" + Period + ""
        resp = requests.get(url=url)
        data = resp.json()

        # Create CSV for every month
        csv_path = './Data/CurrencyPair{}/Period{}/Poloniex_{}.csv'.format(CurrencyPair,Period,x)
        with open(csv_path, 'w', newline='') as f:
            dict_writer = csv.DictWriter(f, data[0].keys())
            dict_writer.writeheader()
            dict_writer.writerows(data)
            print("CSV file saved at path {}".format(csv_path),end="\r")
            f.close()
        now = now - _1month
        now_1month = now - _1month
        time.sleep(1)
    print("")

    # Create all CSVs to one CSV
    alle = []
    for y in range(72):
        csv_path = './Data/CurrencyPair{}/Period{}/Poloniex_{}.csv'.format(CurrencyPair,Period,y)
        with open(csv_path, newline='') as f:
            dict_reader = csv.DictReader(f)
            for row in dict_reader:
                alle.append(row)

    # Save the one CSV
    csv_path = './Data/CurrencyPair{}/Period{}/Poloniex.csv'.format(CurrencyPair,Period)
    with open(csv_path, 'w', newline='') as f:
            dict_writer = csv.DictWriter(f, alle[0].keys())
            dict_writer.writeheader()
            dict_writer.writerows(alle)
            print("CSV file saved at path {}".format(csv_path))

    # Delete all 72 CSVs
    p = mp.Process(target=delcsv, args=(CurrencyPair,Period,))
    p.start()

def delcsv(CurrencyPair,Period):
    for z in range(72):
        csv_path = './Data/CurrencyPair{}/Period{}/Poloniex_{}.csv'.format(CurrencyPair,Period,z)
        os.remove(csv_path)