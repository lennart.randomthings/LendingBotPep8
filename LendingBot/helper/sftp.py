import pysftp
import helper.config
import os

# Init Config
conf = helper.config.initconfig()

myHostname = conf['sftpServer']
myUsername = conf['sftpPW']
myPassword = conf['sftpUser']
cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

def readmodels(localFilePath,CurrencyPair, Period, nPCs, stoploss, takeprofit):
    if not os.path.exists('./Models/CurrencyPair{}'.format(CurrencyPair)):
        os.mkdir('./Models/CurrencyPair{}'.format(CurrencyPair))
    if not os.path.exists('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period)):
        os.mkdir('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
        
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        remotePath = './CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair,Period, nPCs, stoploss, takeprofit)
        if not os.path.exists('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit)):
            os.mkdir('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
        if not os.path.exists('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}/assets'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit)):
            os.mkdir('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}/assets'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
        if not os.path.exists('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}/variables'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit)):
            os.mkdir('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}/variables'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
        
        print(remotePath)
        print(localFilePath)
        sftp.get_d(remotePath, localFilePath)
        sftp.get_d(remotePath + "/assets", localFilePath + "/assets")
        sftp.get_d(remotePath + "/variables", localFilePath + "/variables")
        
    # connection closed automatically at the end of the with-block

def readfile(localFilePath,CurrencyPair,Period):
    if not os.path.exists('./Models/CurrencyPair{}'.format(CurrencyPair)):
        os.mkdir('./Models/CurrencyPair{}'.format(CurrencyPair))
    if not os.path.exists('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period)):
        os.mkdir('./Models/CurrencyPair{}/Period{}'.format(CurrencyPair, Period))
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        remotePath = './CurrencyPair{}/Period{}'.format(CurrencyPair,Period)
        sftp.get_d(remotePath,localFilePath)
        

def writefile(localFilePath,CurrencyPair, Period):
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        # Switch to a remote directory
        sftp.cwd('./CurrencyPair{}/Period{}'.format(CurrencyPair,Period))

        sftp.put(localFilePath)

def writedir(localFilePath,CurrencyPair, Period, nPCs, stoploss, takeprofit):
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        remotePath = './CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair,Period, nPCs, stoploss, takeprofit)
        print(remotePath)
        try:
            sftp.mkdir(remotePath)
        except:
            files = sftp.listdir(remotePath)
            for f in files:
                if sftp.isdir(remotePath + "/" + f):
                    files2 = sftp.listdir(remotePath + "/" + f)
                    for g in files2:
                        sftp.remove(remotePath + "/" + f + "/" + g)
                    sftp.rmdir(remotePath + "/" + f)
                else:
                    sftp.remove(remotePath + "/" + f)

            sftp.rmdir(remotePath)
            sftp.mkdir(remotePath)
        sftp.put_r(localFilePath,remotePath)

def makedir(CurrencyPair, Period):
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        try:
            sftp.mkdir('./CurrencyPair{}'.format(CurrencyPair))
        except:
            pass
        try:
            sftp.mkdir('./CurrencyPair{}/Period{}'.format(CurrencyPair,Period))
        except:
            pass

def checkdir(CurrencyPair, Period, nPCs, stoploss, takeprofit):
    with pysftp.Connection(host=myHostname, username=myUsername, password=myPassword, cnopts=cnopts) as sftp:
        sftp.cwd('/users/hdd-9152/Trading')
        remotePath = './CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair,Period, nPCs, stoploss, takeprofit)
        return sftp.isdir(remotePath)
