from math import isnan
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import keras.models
from keras.models import Sequential
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.callbacks import EarlyStopping
import pickle as pk
from keras.layers import Dense
from scipy.stats import ttest_1samp
import json
import helper.sftp

def compute_sma(df, window, colname):
    '''Computes Simple Moving Average column on a dataframe'''
    df[colname] = df['close'].rolling(window=window, center=False).mean()
    return(df)

def compute_rsi(df, window, colname):
    '''Computes RSI column for a dataframe. http://stackoverflow.com/a/32346692/3389859'''
    series = df['close']
    delta = series.diff().dropna()
    u = delta * 0
    d = u.copy()
    u[delta > 0] = delta[delta > 0]
    d[delta < 0] = -delta[delta < 0]
    # first value is sum of avg gains
    u[u.index[window - 1]] = np.mean(u[:window])
    u = u.drop(u.index[:(window - 1)])
    # first value is sum of avg losses
    d[d.index[window - 1]] = np.mean(d[:window])
    d = d.drop(d.index[:(window - 1)])
    rs = u.ewm(com=window - 1,ignore_na=False,
               min_periods=0,adjust=False).mean() / d.ewm(com=window - 1, ignore_na=False,
                                            min_periods=0,adjust=False).mean()
    df[colname] = 100 - 100 / (1 + rs)
    df[colname].fillna(df[colname].mean(), inplace=True)
    return(df)

def compute_variables1(df):
    print("Let's compute predictive variables : ")
    df["date"] = pd.to_datetime(df["date"])
    df['close'] = pd.to_numeric(df['close'])
    df['open'] = pd.to_numeric(df['open'])
    df['bodysize'] = df['close'] - df['open']
    df['high'] = pd.to_numeric(df['high'])
    df['low'] = pd.to_numeric(df['low'])
    df['shadowsize'] = df['high'] - df['low']
    #TODO evtl Reihe erhöhen und auf auswirkungen schauen
    for window in [3, 8, 21, 55, 144, 377]: # several Fibonacci numbers
        # SMA
        df = compute_sma(df, window, colname = 'sma_{}'.format(window))
        # RSI
        df = compute_rsi(df, window, colname = 'rsi_{}'.format(window))
        # Values
        df["Min_{}".format(window)] = df["low"].rolling(window).min()
        df["Max_{}".format(window)] = df["high"].rolling(window).max()
        df["volume_{}".format(window)] = df["volume"].rolling(window).mean()
        df['percentChange_{}'.format(window)] = df['close'].pct_change(periods = window)
        df['RelativeSize_sma_{}'.format(window)] = df['close'] / df['sma_{}'.format(window)]
        df['Diff_{}'.format(window)] = df['close'].diff(window)

    # Add modulo 10, 100, 1000, 500, 50
    df["Modulo_10"] = df["close"].copy() % 10
    df["Modulo_100"] = df["close"].copy() % 100
    df["Modulo_1000"] = df["close"].copy() % 1000
    df["Modulo_500"] = df["close"].copy() % 500
    df["Modulo_50"] = df["close"].copy() % 50

    # Add weekday and day of the month
    df["WeekDay"] = df["date"].dt.weekday
    df["Day"] = df["date"].dt.day

    # Remove na values
    df.dropna(inplace=True)
    return(df)

def check_outcome(df, line, stoploss, takeprofit):
    '''0 means we reached stoploss
    1 means we reached takeprofit
    -1 means still in between'''
    price0 = df["close"].iloc[line]
    upper_lim = price0*(1+takeprofit)
    down_lim = price0*(1-stoploss)
    for i in range(line, df["close"].size):
        if df["low"].iloc[i] < down_lim :
            return(0)
        elif df["high"].iloc[i] > upper_lim :
            return(1)
    return(-1)

from multiprocessing import Pool

def f(df, stoploss, takeprofit):
    print('start')
    tmp = []
    for i in range(df["close"].size):
        # df['result'].iloc[i] = check_outcome(df, i, stoploss, takeprofit)
        tmp.append(check_outcome(df, i, stoploss, takeprofit))
    print("end")
    df['result'] = tmp
    return df

# https://docs.python.org/3/library/multiprocessing.html
def compute_result(df, stoploss, takeprofit):
    global dfs
    p = Pool(8)
    df['result'] = 0
    n_worker = 50
    N = df["close"].size
    dfs = []

    print("####",df.shape)

    def adder(_df):
        global dfs
        dfs.append(_df)

    for i in range(n_worker):
        print(int(i*N/n_worker),int((i+1)*N/n_worker))
        _df = df.iloc[int(i*N/n_worker):int((i+1)*N/n_worker)]
        # print(_df.head())
        p.apply_async(f, args=(_df, stoploss, takeprofit,), callback=adder)
    p.close()
    p.join()

    df = pd.concat(dfs)
    print("CONCATENATION",df.shape)
    return (df)


def compute_earnings_loss(stoploss, takeprofit, fees):
    '''Compute earnings and loss with given fees, stoploss, takeprofit'''
    win = (1-fees)*(1+takeprofit)*(1-fees) -1
    loss = (1-fees)*(1-stoploss)*(1-fees) -1
    return(win, loss)

def predict_and_backtest_bullish(df, df_final, model, stoploss, takeprofit, fees, nPCs, plotting):
    '''This functin takes the test set as input (in both shapes) + the model, computes predictions and  probabilities, then compute the earnings according to the fees. Finally it can plot the strategy'''
    # Compute predictions on testset
    clf = model
    df['preds'] = (clf.predict(df_final.iloc[:, :nPCs]) > 0.5)*1
    df['proba1'] = clf.predict(df_final.iloc[:, :nPCs])
    print(df['proba1'])

    # keep only the timesteps in which the model predicts a bullish trend
    testset1 = df[df['preds'] == 1].copy()

    # Compute earnings column
    a = compute_earnings_loss(stoploss, takeprofit, fees)
    testset1['EarningsBullish'] = (testset1['preds'] == testset1['result'])*a[0] + (testset1['preds'] != testset1['result'])*a[1]

    # if plotting:
    #     # Now plot our trading strategy
    #     plt.plot(pd.to_datetime(testset1['date']), np.cumsum(testset1['EarningsBullish']))
    #     plt.title('Approach over the test set \n ROI = {} %'.format(100*np.mean(testset1['EarningsBullish'])))
    #     plt.xlabel('Date')
    #     plt.xlabel('Cumulative Earnings')
    #     plt.show()

    #     # Display the entry points
    #     plt.plot(pd.to_datetime(df['date']), df['close'])
    #     plt.scatter(pd.to_datetime(testset1['date']), testset1['close'], c = (testset1['EarningsBullish']>0))
    #     plt.title('Entry points \n Yellow = Win, Blue = Loss')
    #     plt.show()

    return(testset1)

def table_recap(df, stoploss, takeprofit, nPCs, columnA = 'proba1', columnB = 'EarningsBullish'):
    ''' Summarize the strategy by steps of 0.05, depending on which column (i.e. strategy)
    we choose'''
    recap = pd.DataFrame(np.zeros((int(10), 0)))
    recap['stoploss'] = stoploss
    recap['takeprofit'] = takeprofit
    recap['nPCs'] = nPCs
    recap['Min'] = [0.5 + k*0.05 for k in range(10)]
    recap['Max'] = 1
    recap['ROI%'] = 0
    recap['nTrades'] = 0
    for i in range(len(recap['Min'])):
        min, max = recap['Min'].iloc[i], recap['Max'].iloc[i]
        df2 = df[(df[columnA] > min) & (df[columnA] < max)]
        recap['ROI%'].iloc[i] = 100 * np.mean(df2[columnB])
        recap['nTrades'].iloc[i] = df2.shape[0]

    return(recap)

def sortdata(CurrencyPair,Period):
    # Read the CSV
    df = pd.read_csv('./Data/CurrencyPair{}/Period{}/Poloniex.csv'.format(CurrencyPair, Period))
    
    # Convert the UNIX time into the date
    df["date"] = pd.to_datetime(df["date"],unit='s')

    # Sort the data by date
    df = df.sort_values(by = 'date')

    # Use only the important values
    df = df[['close', 'date', 'high', 'low', 'open', 'volume']]

    # Compute Variables
    df = compute_variables1(df)

    # Save to CSV
    df.to_csv('./Data/CurrencyPair{}/Period{}/DatasetWithVariables.csv'.format(CurrencyPair, Period), index = False)


def compute(CurrencyPair,Period,stoploss,takeprofit):
    # Read the CSV
    df = pd.read_csv('./Data/CurrencyPair{}/Period{}/DatasetWithVariables.csv'.format(CurrencyPair, Period))
    
    # Compute the results
    df = compute_result(df, stoploss, takeprofit)
    
    # delete all they have no result
    df = df[df['result']>=0] # Only keep observations where we also have the result
    
    # delete all they have infinity
    df = df[df['percentChange_377']<np.inf]

    # Sort data by date
    df = df.sort_values(by = 'date')

    # Save to CSV
    df.to_csv('./Data/CurrencyPair{}/Period{}/DatasetWithVariablesAndY_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)

def prepairtrain(Database,CurrencyPair,Period,stoploss,takeprofit):
    # Read the CSV
    df = pd.read_csv('./Data/CurrencyPair{}/Period{}/DatasetWithVariablesAndY_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
    
    # Search the len of the Dataframe
    k = df.shape[0]

    # 25% to trainset
    trainset = df[:int(k*.60)] #df[df['date'] < start_validation]
    # 50% to validationset
    validation_set = df[int(k*.60):int(k*.80)] #df[(df['date'] >= start_validation) & (df['date'] < start_test)]
    # 25% to testset
    testset = df[int(k*.80):] #df[df['date'] > start_test]

    # Save trainset to CSV
    trainset.to_csv('./Data/CurrencyPair{}/Period{}/TrainSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)
    # Save calidationset to CSV
    validation_set.to_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)
    # Save testset to CSV
    testset.to_csv('./Data/CurrencyPair{}/Period{}/TestSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)

    # Init the Pickle Data
    data = {}
    data['CurrencyPair'] = CurrencyPair
    data['takeprofit'] = takeprofit
    data['stoploss'] = stoploss

    #Create SFTP Dir
    helper.sftp.makedir(CurrencyPair, Period)

    # Scale the variables
    scale_fct = StandardScaler()
    scale_fct.fit(trainset.drop(columns='date').drop(columns='result'))
    localfilePath = './Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_scaler.pkl'.format(CurrencyPair, Period, stoploss, takeprofit)
    pk.dump(scale_fct, open(localfilePath,'wb'))
    helper.sftp.writefile(localfilePath, CurrencyPair, Period)

    # Apply PCA
    pca = PCA(n_components=trainset.shape[1] - 2) # remove the result and the date
    pca.fit(scale_fct.transform(trainset.drop(columns='date').drop(columns='result')))
    localfilePath = './Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_pca.pkl'.format(CurrencyPair, Period, stoploss, takeprofit)
    pk.dump(pca, open(localfilePath,"wb"))
    helper.sftp.writefile(localfilePath, CurrencyPair, Period)

    # Scale PCA components (this accelerates training process in Deep Learning)
    pca_scaler = StandardScaler()
    pca_scaler.fit(pca.transform(scale_fct.transform(trainset.drop(columns='date').drop(columns='result'))))
    localfilePath = './Models/CurrencyPair{}/Period{}/stoploss{}_takeprofit{}_pca_scaler.pkl'.format(CurrencyPair, Period, stoploss, takeprofit)
    pk.dump(pca_scaler, open(localfilePath,'wb'))
    helper.sftp.writefile(localfilePath, CurrencyPair, Period)
    

    # Save all in the Database
    # Database.insertPickle(data)

    # Transform to ready to use sets
    trainset_final = pd.DataFrame(pca_scaler.transform(pca.transform(scale_fct.transform(trainset.drop(columns='date').drop(columns='result')))))
    validation_set_final = pd.DataFrame(pca_scaler.transform(pca.transform(scale_fct.transform(validation_set.drop(columns='date').drop(columns='result')))))
    testset_final = pd.DataFrame(pca_scaler.transform(pca.transform(scale_fct.transform(testset.drop(columns='date').drop(columns='result')))))

    # Save sets into CSV
    trainset_final.to_csv('./Data/CurrencyPair{}/Period{}/TrainSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)
    validation_set_final.to_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period,stoploss, takeprofit), index = False)
    testset_final.to_csv('./Data/CurrencyPair{}/Period{}/TestSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)

def train(Database,CurrencyPair,Period,stoploss,takeprofit,nPCs):

    # Load trainset and validationset from CSV
    trainset_final = pd.read_csv('./Data/CurrencyPair{}/Period{}/TrainSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
    trainset = pd.read_csv('./Data/CurrencyPair{}/Period{}/TrainSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))

    # Build a trainset wth numbers of dims (nPCs)
    print(nPCs)
    X = trainset_final.iloc[:, :nPCs]
    y = trainset["result"]

    # EarlyStopping
    custom_early_stopping = EarlyStopping(
        monitor='accuracy', 
        patience=8, 
        min_delta=0.001, 
        mode='max'
        )
    
    # Build model and train it
    classifier = Sequential()

    # First Hidden Layer
    classifier.add(Dense(32, activation='relu', kernel_initializer='random_normal', input_dim=nPCs))
    
    # Second, third and fourth  hidden Layers
    classifier.add(Dense(32, activation='relu', kernel_initializer='random_normal'))
    classifier.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    classifier.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    classifier.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    classifier.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    
    # Output Layer
    classifier.add(Dense(1, activation='sigmoid', kernel_initializer='random_normal'))

    # Compiling the neural network
    classifier.compile(optimizer ='adam',loss='binary_crossentropy', metrics =['accuracy'])
    
    # Fitting the data to the training dataset
    classifier.fit(X,y, batch_size=500, epochs=200, verbose =2, callbacks=[custom_early_stopping])
    
    # Save Model to File
    localfilePath = './Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit)
    classifier.save(localfilePath)
    helper.sftp.writedir(localfilePath,CurrencyPair,Period, nPCs, stoploss, takeprofit)

    # Save Model to Database
    data = {}
    data['CurrencyPair'] = CurrencyPair
    data['stoploss'] = stoploss
    data['takeprofit'] = takeprofit
    data['nPCs'] = nPCs
    #data['Model'] = classifier.to_json()
    #data['pickle'] = scale
    data['finish'] = False
    Database.insertModels(data)
    del(classifier)

def test(Database, CurrencyPair,Period,stoploss,takeprofit,nPCs):
    # Read validationset final from CSV
    validation_set_final = pd.read_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
    # Read validationset from CSV
    validation_set = pd.read_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))

    # (c) Test onto the testset : we compare all models and store results in a csv file
    #accuracies, nPCs_list = [], []
    print("Test: {}".format(nPCs))
    
    # Load Model from Database
    # model = Database.requestModels(CurrencyPair,stoploss,takeprofit,nPCs)['Model']
    # clf = keras.models.model_from_json(model)
    clf = keras.models.load_model('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
    
    # Compute predictions on testset
    preds = (clf.predict(validation_set_final.iloc[:, :nPCs]) > 0.5)*1

    # Assess accuracy on Bullish predictions only (because we will only perform Bullish trades IRL) : we prioritize selectivity
    validation_set1 = validation_set[preds == 1].copy()
    # accuracies.append(np.mean(preds == list(validation_set1['result'])))
    # nPCs_list.append(nPCs)
    data={}
    data['CurrencyPair'] = CurrencyPair
    data['nPCs'] = nPCs
    data['takeprofit'] = takeprofit
    data['stoploss'] = stoploss
    if isnan(np.mean(preds == list(validation_set1['result']))):
        data['accuracies'] = 0.0
    else:
        data['accuracies'] = np.mean(preds == list(validation_set1['result']))
    Database.addtoModels(data)

    #recap = pd.DataFrame({'nPCs' : list(nPCs_list), 'Accuracy' : (list(accuracies))})
    #recap.to_csv('./Results/CurrencyPair{}/Period{}/Comparative_All_models_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit), index = False)
    #print(recap)

def ROIperform(CurrencyPair,Period,stoploss,takeprofit,Database):

    ################### (a) Load previously built datasets
    fees = 0.00155 # transaction fees : 0.125% for example

    testset_final = pd.read_csv('./Data/CurrencyPair{}/Period{}/TestSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
    testset = pd.read_csv('./Data/CurrencyPair{}/Period{}/TestSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))

    #################### (b) Basic strategy : pick the best model and bet on bullish trends over the testset
    #TODO test it with all nPCs
    recap = pd.read_csv('./Results/CurrencyPair{}/Period{}/Comparative_All_models_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit)).sort_values('Accuracy', ascending = False)
    nPCs = recap['nPCs'].iloc[0]

    # with open("./Models/DL_model_{}PC_stoploss{}_takeprofit{}.pkl".format(nPCs, stoploss, takeprofit), 'rb') as f:
    #     clf = pk.load(f)
    clf = Sequential()
    #First Hidden Layer
    #TODO Läuft hier Deep Leraning überhaupt?
    clf.add(Dense(32, activation='relu', kernel_initializer='random_normal', input_dim=nPCs))
    #Second, third and fourth  hidden Layers
    clf.add(Dense(32, activation='relu', kernel_initializer='random_normal'))
    clf.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    clf.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    clf.add(Dense(16, activation='relu', kernel_initializer='random_normal'))
    clf.add(Dense(16, activation='relu', kernel_initializer='random_normal'))

    #Output Layer
    clf.add(Dense(1, activation='sigmoid', kernel_initializer='random_normal'))
    # with open("./Models/DL_model_{}PC_stoploss{}_takeprofit{}.pkl".format(nPCs, stoploss, takeprofit), 'rb') as f:
    #     clf = pk.load(f)
    clf.load_weights('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}.h5'.format(CurrencyPair, Period,nPCs, stoploss, takeprofit))
    # Compute predictions on testset

    testset1 = predict_and_backtest_bullish(testset, testset_final, clf, stoploss, takeprofit, fees, nPCs, False)

    # Assess the performance by comparing to if we always traded bullish blindly over the period
    a = compute_earnings_loss(stoploss, takeprofit, fees)
    testset_benchmark = testset.copy()
    testset_benchmark['EarningsBullish'] = (testset['result'] == 1)*a[0] + (testset['result'] == 0)*a[1]
    avg_return_benchmark = np.mean(testset_benchmark['EarningsBullish'])

    # Now let's look at our approach's performance and std
    p_value = ttest_1samp(testset1['EarningsBullish'], popmean = avg_return_benchmark)[1]

    data = {}
    data['CurrencyPair'] = CurrencyPair
    data['nPCs'] = nPCs
    data['takeprofit'] = takeprofit
    data['stoploss'] = stoploss
    data['p_value'] = p_value
    data['avg_return_benchmark'] = 100*avg_return_benchmark
    data['ROI'] = 100*np.mean(testset1['EarningsBullish'])
    Database.insertROI(data)

    print('Our model has an average ROI of {} %, while trading blindly bullish over the same period yielded a ROI of {} %, when we perform statistical testing of difference there is a p-value of {}.'.format(100*np.mean(testset1['EarningsBullish']), 100*avg_return_benchmark, p_value))

def search_the_best(Database,CurrencyPair,Period,stoploss,takeprofit,nPCs):
    
    data = {}

    # Init the transaction fee
    fees = 0.00155 # transaction fees : 0.125% for example
    
    # Init the minimal number of trades in test set
    nTrades_mini = 50 # minimal number of trades we want over the test set: this is for second approach

    # Load Model from Database
    #model = Database.requestModels(CurrencyPair,stoploss,takeprofit,nPCs)['Model']
    #clf = keras.models.model_from_json(model)
    clf = keras.models.load_model('./Models/CurrencyPair{}/Period{}/DL_model_{}PC_stoploss{}_takeprofit{}'.format(CurrencyPair, Period, nPCs, stoploss, takeprofit))
    # Read Read validationset final from CSV
    validation_set_final = pd.read_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
    # Read validationset from CSV
    validation_set = pd.read_csv('./Data/CurrencyPair{}/Period{}/ValidationSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))

    # Compute predictions on validation_set
    validation_set = predict_and_backtest_bullish(validation_set, validation_set_final, clf, stoploss, takeprofit, fees, nPCs, plotting = False)

    # Compute recapitulative table
    recap = table_recap(validation_set, stoploss, takeprofit, nPCs)

    # Pick the most profitable
    recap = recap.sort_values('ROI%', ascending = False)
    recap = recap[recap['nTrades'] > nTrades_mini]
    if not recap.empty:
        min, max = recap['Min'].iloc[0], recap['Max'].iloc[0]

        # Save into Testset_final and testset CSV
        testset_final = pd.read_csv('./Data/CurrencyPair{}/Period{}/TestSet_final_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))
        testset = pd.read_csv('./Data/CurrencyPair{}/Period{}/TestSet_stoploss{}_takeprofit{}.csv'.format(CurrencyPair, Period, stoploss, takeprofit))

        # Compute predictions on testset
        testset2 = predict_and_backtest_bullish(testset, testset_final, clf, stoploss, takeprofit, fees, nPCs, plotting = False)
        testset2 = testset2[(testset2['proba1'] > min) & (testset2['proba1'] < max)].copy()

        # (iii) Assess the performance by comparing to if we always traded bullish over the period
        a = compute_earnings_loss(stoploss, takeprofit, fees)
        testset_benchmark = testset.copy()
        testset_benchmark['EarningsBullish'] = (testset['result'] == 1)*a[0] + (testset['result'] == 0)*a[1]
        avg_return_benchmark = np.mean(testset_benchmark['EarningsBullish'])

        # Save all datas in Database
        p_value = ttest_1samp(testset2['EarningsBullish'], popmean = avg_return_benchmark)[1]

        if isnan(p_value):
            data['p_value'] = 0.0
        else:
            data['p_value'] = p_value
        data['min'] = min
        data['max'] = max
        data['recap'] = json.loads(recap.to_json())
        data['avg_return_benchmark'] = 100*avg_return_benchmark
        if isnan(100*np.mean(testset2['EarningsBullish'])):
            data['ROI'] = 0.0
        else:
            data['ROI'] = 100*np.mean(testset2['EarningsBullish'])
        print('Our model has an average ROI of {} %, while trading blindly bullish over the same period yielded a ROI of {} %, when we perform statistical testing of difference there is a p-value of {}.'.format(100*np.mean(testset2['EarningsBullish']), 100*avg_return_benchmark, p_value))

    data['CurrencyPair'] = CurrencyPair
    data['nPCs'] = nPCs
    data['takeprofit'] = takeprofit
    data['stoploss'] = stoploss

    data['finish'] = True
    Database.addtoModels(data)
    del(clf)