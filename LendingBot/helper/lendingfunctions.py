import poloniex
import CLASS.POLONIEXAPI
import helper.config
import time
from datetime import datetime,timedelta

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()

BTCmin = 0.005
USDTmin = 50.0
ATOMmin = 10
TRXmin = 2500

def lendcheck(AvailableBalancedata, OpenLoans):
    Poloniexapi.init()
    if OpenLoans != []:
        for value1 in OpenLoans:
            for value2 in OpenLoans[value1]:
                present = datetime.utcnow()
                future  = datetime.strptime(value2["date"], '%Y-%m-%d %H:%M:%S')# + timedelta(days = value2['duration'])
                difference = present - future
                if difference > timedelta(hours=6):
                    Poloniexapi.cancelLoanOffer(value2['id'])
    if 'lending' in AvailableBalancedata:
        #print(AvailableBalancedata['lending'])
        for key in AvailableBalancedata['lending']:
            lend(key, float(AvailableBalancedata['lending'][key]), OpenLoans, Poloniexapi)

def lend(Currency, Balance, OpenLoans, Poloniexapi):
    if Currency == 'BTC':
        if 'BTC' in OpenLoans:
            for x in OpenLoans['BTC']:
                Poloniexapi.cancelLoanOffer(int(x['id']))
        if float(Balance) > BTCmin:
            createLoan(Currency,Balance,BTCmin)
    if Currency == 'USDT':
        if 'USDT' in OpenLoans:
            for x in OpenLoans['USDT']:
                Poloniexapi.cancelLoanOffer(int(x['id']))
        if float(Balance) > USDTmin:
            createLoan(Currency,Balance,USDTmin)
    if Currency == 'ATOM':
        if 'ATOM' in OpenLoans:
            for x in OpenLoans['ATOM']:
                Poloniexapi.cancelLoanOffer(int(x['id']))
        if float(Balance) > ATOMmin:
            createLoan(Currency,Balance,ATOMmin)
    if Currency == 'TRX' and float(Balance) > TRXmin-102:
        if 'TRX' in OpenLoans:
            for x in OpenLoans['TRX']:
                Poloniexapi.cancelLoanOffer(int(x['id']))
        if float(Balance) > TRXmin-101:
            createLoan(Currency,Balance-101,TRXmin)

def createLoan(Currency,Balance,minimum):
    conf = helper.config.initconfig()
    #print(Currency)
    avg = Poloniexapi.avgLoanOrders(Currency)
    completeammount = Balance
    print("Genug " + Currency + " zum Lending")
    #print("{:.8f} ".format(avg) + Currency)
    #print(completeammount)
    if conf['activeLending'] == "True":
        if float(Balance) > minimum*3:
            ammount = completeammount/3
            Poloniexapi.createLoanOffer(Currency, ammount,avg)
            Poloniexapi.createLoanOffer(Currency, ammount,avg*0.95)
            Poloniexapi.createLoanOffer(Currency, ammount,avg*1.02)
        if float(Balance) > minimum*2:
            ammount = completeammount/2
            Poloniexapi.createLoanOffer(Currency, ammount,avg)
            Poloniexapi.createLoanOffer(Currency, ammount,avg*0.95)
        elif float(Balance) > minimum:
            Poloniexapi.createLoanOffer(Currency, completeammount,avg*0.95)