# Deployment functions
from poloniex import Poloniex
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
import poloniex
import os
from time import time, sleep
import pickle as pk
import re
import helper.config

conf = helper.config.initconfig()


def buy_asset(pair, amount, store = True):
    '''Given a pair and an amount in $, buy the amount at the lowest possible price'''

    polo = Poloniex(conf["api_key"], conf["secret"])

    total_bought = 0
    total_to_buy = amount
    res = []
    total = 0
    # I - Sell
    while total_bought < 0.95*amount: # Let's say 95% is acceptable : in the vast majority of cases we will achieve 100% with the first trade, though...
        # Order to buy the missing amount we want to sell, then update the total amount bought
        rate = polo.returnTicker()[pair]["lowestAsk"]
        amount_pair = total_to_buy/float(rate)
        res2 = polo.buy(currencyPair=pair, rate= rate, amount = amount_pair, orderType = "immediateOrCancel")
        trade = res2['resultingTrades']
        total = [float(trade[k]['total']) for k in range(len(trade))]
        total_bought = total_bought + np.sum(total)
        total_to_buy = total_to_buy - np.sum(total)
        # Update the recapitulative of trades passed
        res = res + [res2]

    # II - Create a recapitulative table
    trades = [res[k]['resultingTrades'] for k in range(len(res))]
    print("len trades: " + str(len(trades)))
    dates = [trades[i][0]['date'] for i in range(len(trades))]
    amounts = [trades[i][0]['takerAdjustment'] for i in range(len(trades))] # amount bought expressed in the pair
    totals = [trades[i][0]['total'] for i in range(len(trades))] #amounts expressed in dollar
    rates = [trades[i][0]['rate'] for i in range(len(trades))] #rate of the pair for each trade

    fees = float(res[0]['fee'])

    recap= pd.DataFrame({'Date' : dates, 'AmountInCurrency' : amounts, 'Total$' : totals, 'Rate' : rates})
    recap['FeesPaid$'] = float(recap['Total$'])*fees
    recap['orderNumber'] = res[0]['orderNumber']

    # III - If selected, store the trades in a csv
    if store:
        if not os.path.exists('./Trades'):
            os.makedirs('./Trades')
        recap.to_csv('./Trades/Buy_{}_{}_{}.csv'.format(pair, amount, res[0]['orderNumber']), index = False, sep = ';')

    print('We bought {} of {}. '.format(total_bought, pair))
    return(recap)

def sell_asset(pair, amount, store = True):
    '''Given a pair and an amount expressed in the pair, sell the amount at the highest possible price'''

    polo = Poloniex(conf["api_key"], conf["secret"])

    amount_sold = 0
    amount_to_sell = amount
    res = []
    amount1 = 0
    # I - Sell
    while amount_sold < 0.95*amount: # Let's say 95% is acceptable : in the vast majority of cases we will achieve 100% with the first trade, though...
        # Order to sell the missing amount we want to sell, then update the total amount sold
        res2 = polo.sell(currencyPair= pair, rate=polo.returnTicker()[pair]["highestBid"], amount=amount_to_sell, orderType = False)
        trade = res2['resultingTrades']
        amount1 = [float(trade[k]['amount']) for k in range(len(trade))]
        amount_sold = amount_sold + np.sum(amount1)
        amount_to_sell = amount_to_sell - np.sum(amount1)
        # Update the recapitulative of trades passed
        res = res + [res2]

    # II - Create a recapitulative table
    trades = [res[k]['resultingTrades'] for k in range(len(res))]

    dates = [trades[i][0]['date'] for i in range(len(trades))]
    amounts = [trades[i][0]['amount'] for i in range(len(trades))] # amounts expressed in the pair
    totals = [trades[i][0]['total'] for i in range(len(trades))] #amounts expressed in dollar
    totals_adjusted = [trades[i][0]['takerAdjustment'] for i in range(len(trades))] # amounts minus fees expressed in dollar
    rates = [trades[i][0]['rate'] for i in range(len(trades))] #rate of the pair for each trade

    recap= pd.DataFrame({'Date' : dates, 'AmountInCurrency' : amounts, 'Total$' : totals,'Total$Adjusted': totals_adjusted, 'Rate' : rates})
    recap['amount_sold'] = amount_sold
    recap['totals'] = totals
    recap['FeesPaid$'] = float(recap['Total$']) - float(recap['Total$Adjusted'])
    recap['Pair'] = pair
    recap['Type'] = 'Sell'

    print('We sold {} of {}. '.format(amount_sold, pair))
    return(recap)


def track_investment(pair, price, amount, stoploss_price, takeprofit_price):

    polo = Poloniex(conf["api_key"], conf["secret"])

    print('stoploss_price = ', stoploss_price, 'takeprofit_price = ', takeprofit_price)
    while True:
        if float(polo.returnTicker()[pair]["lowestAsk"]) < stoploss_price :
            return(sell_asset(pair, amount, store = True)) # Sell if we reached the stoploss
        if float(polo.returnTicker()[pair]["highestBid"]) > takeprofit_price :
            return(sell_asset(pair, amount, store = True)) # Sell if we reached the takeprofit
        print('{} Latest price is'.format(datetime.now()), polo.returnTicker()[pair]['last'], 'stoploss_price = ', stoploss_price, 'takeprofit_price = ', takeprofit_price,end="\r")
        sleep(10)

def sell_everything(currency):

    polo = Poloniex(conf["api_key"], conf["secret"])

    pair = 'USDT_{}'.format(currency)
    recap = sell_asset(pair = pair, amount = polo.returnBalances()[currency])
    return(recap)