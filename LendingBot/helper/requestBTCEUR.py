import requests

def request():
    url = "https://blockchain.info/ticker"
    resp = requests.get(url=url)
    data = resp.json()
    return data["EUR"]["last"]