import CLASS.DATABASE
import CLASS.POLONIEXAPI
import logging
import plotly.express as px
import os
from flask import Flask , render_template, request
import pandas as pd
import json
import helper.config
import datetime

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()
Poloniexapi.init()

app = Flask(__name__)

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Lending.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

conf = helper.config.initconfig()

liste = helper.config.returnListofCurrency()

Database = CLASS.DATABASE.DATABASE()
Database.connect()
Database.setup()

def createplot(Currency_Pair):
    Data = Database.requestTicker(0,Currency_Pair)

    x = []
    y = []

    for date in Data:
        x.append(date['date'])
        y.append(date['currency']['last'])

    if not os.path.exists('LendingBot/templates/plots'):
            os.makedirs('LendingBot/templates/plots')

    fig = px.line( x=x, y=y, title=Currency_Pair)
    fig.write_html("LendingBot/templates/plots/" + Currency_Pair + ".html")


@app.route('/')
def index():
    Currency = {}
    for Cur in liste:
        Currency[Cur] = {}
        Currency[Cur]['name'] = Cur
        Currency[Cur]['last'] = Database.requestLastPrice(Cur)[0]['last']
        Currency[Cur]['date'] = Database.requestLastPrice(Cur)[0]['date']
        #Currency[Cur]['volume'] = Poloniexapi.volume(Cur)
        model = Database.requestBest(Cur)
        if len(model) >0:
            Currency[Cur]['best'] = model[0]
        else:
            Currency[Cur]['best'] = {}
            Currency[Cur]['best']['stoploss'] = 0
            Currency[Cur]['best']['takeprofit'] = 0
            Currency[Cur]['best']['nPCs'] = 0
            Currency[Cur]['best']['ROI'] = 0
            Currency[Cur]['best']['accuracies'] = 0
            Currency[Cur]['best']['min'] = 0
        Trainer = Database.requestTrainer(Cur)
        if Trainer:
            Currency[Cur]['TrainerStatus'] = Trainer
        else:
            Currency[Cur]['TrainerStatus'] = {}
            Currency[Cur]['TrainerStatus']['Status'] = "unknown"
            Currency[Cur]['TrainerStatus']['date'] = datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%d %H:%M:%S")

    return render_template('index.html',
                                Currency = Currency,
                                )

@app.route('/trader')
def trader():
    Currency = {}
    for Cur in liste:
        Currency[Cur] = {}
        Currency[Cur]['name'] = Cur
        Currency[Cur]['last'] = Database.requestLastPrice(Cur)[0]['last']
        Currency[Cur]['date'] = Database.requestLastPrice(Cur)[0]['date']
        Currency[Cur]['TraderStatus'] = {}
        #Currency[Cur]['volume'] = Poloniexapi.volume(Cur)
        model = Database.requestBest(Cur)
        if len(model) >0:
            Currency[Cur]['best'] = model[0]
        else:
            Currency[Cur]['best'] = {}
            Currency[Cur]['best']['stoploss'] = 0
            Currency[Cur]['best']['takeprofit'] = 0
            Currency[Cur]['best']['nPCs'] = 0
            Currency[Cur]['best']['ROI'] = 0
            Currency[Cur]['best']['accuracies'] = 0
            Currency[Cur]['best']['min'] = 0
        Trader = Database.requestTrader(Cur,300)
        for i in Trader:
            Currency[Cur]['TraderStatus'][i['index']] = i
        print(Currency[Cur])

    return render_template('trader.html',
                                Currency = Currency,
                                )


@app.route('/plots/pair')
def chart1():
    CurrencyPair = request.args['pair']
    createplot(CurrencyPair)
    return render_template('plots/{}.html'.format(CurrencyPair))

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


app.run(host='0.0.0.0', debug=True, port=5000)