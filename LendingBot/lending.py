import CLASS.POLONIEXAPI
import helper.config
import helper.telegramsend
import helper.requestBTCEUR
import CLASS.DATABASE
import helper.lendingfunctions
from datetime import datetime,timedelta
import os
import time
import logging

Poloniexapi = CLASS.POLONIEXAPI.POLONIEXAPI()

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.FileHandler("logs/Lending.log")
formatter = logging.Formatter('%(asctime)s:%(levelname)s-%(message)s')
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

conf = helper.config.initconfig()

if conf['Database'] == "True":
    Database = CLASS.DATABASE.DATABASE()
    Database.connect()
    Database.setup()

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

def printscreen():
    Tickerdata = Poloniexapi.ticker
    AvailableBalancedata = Poloniexapi.availableAccountBalances
    ActiveLoans = Poloniexapi.activeLoans
    OpenLoans = Poloniexapi.OpenLoanOffers
    BTCEUR = helper.requestBTCEUR.request()
    #CompleteBalance = Poloniexapi.completeBalances
    cls()
    print("LENDING BOT")
    print("USDT_BTC: " + Tickerdata["USDT_BTC"]["last"])
    print("")
    if 'lending' in AvailableBalancedata:
        for key in AvailableBalancedata["lending"]:
            if float(AvailableBalancedata["lending"][key]) > 0.0:
                print("You have " + AvailableBalancedata["lending"][key] + " " + str(key) + " available")
    else:
        print("You have nothing available")
    print("")
    if ActiveLoans != []:
        print("Active Loans")
        for key in ActiveLoans["provided"]:
            present = datetime.utcnow()
            future  = datetime.strptime(key["date"], '%Y-%m-%d %H:%M:%S') + timedelta(days = key['duration'])
            difference = future - present
            print(key["currency"] + ": Rate: " + key["rate"] + "% Amount: " + key ["amount"] + " Time to End: " + str(difference))
    print("")
    if OpenLoans != []:
        print("Open Loans")
        for value1 in OpenLoans:
            for value2 in OpenLoans[value1]:
                present = datetime.utcnow()
                future  = datetime.strptime(value2["date"], '%Y-%m-%d %H:%M:%S')# + timedelta(days = value2['duration'])
                difference = present - future
                print(value1 + ": Rate: " + value2["rate"] + "% Amount: " + value2 ["amount"] + " Time since create: " + str(difference))
    print("")
    print(str(Poloniexapi.completeBalancesSum()) + " BTC")
    print(str(Poloniexapi.completeBalancesSum()*BTCEUR) + " €")


def check():
    AvailableBalancedata = Poloniexapi.availableAccountBalances
    ActiveLoans = Poloniexapi.activeLoans
    time.sleep(1)
    HistoryLoans = Poloniexapi.LendingHistory
    HistoryTrade = Poloniexapi.TradeHistory
    time.sleep(1)
    OpenLoans = Poloniexapi.OpenLoanOffers
    helper.lendingfunctions.lendcheck(AvailableBalancedata, OpenLoans)
    if 'TRX' not in AvailableBalancedata['exchange'] or float(AvailableBalancedata['exchange']['TRX']) < 100.0:
        if float(AvailableBalancedata['lending']['TRX']) > 100:
            Poloniexapi.transferBalance('TRX',100,'lending','exchange')
            helper.telegramsend.send("100 TRX Tranfer from Lending to Exchange")
        else:
            helper.telegramsend.send("Not Enoug TRX for pay Fee")
    
    if conf['Database'] == "True":
        for key in ActiveLoans["provided"]:
            Text = Database.insertActiveLoan(key)
            if len(Text) > 1 :
                helper.telegramsend.send(Text)
        Text = Database.insertHistoryLoan(HistoryLoans)
        if len(Text) > 1 :
            helper.telegramsend.send(Text)
        Text = Database.insertHistoryTrade(HistoryTrade)
        if len(Text) > 1 :
            helper.telegramsend.send(Text)
        Database.insertTicker(Poloniexapi.ticker)
        # TODO kann raus
        # Database.insertBTC(Poloniexapi.ticker["USDT_BTC"]["last"])
    
    
    
while True:
    try:
        conf = helper.config.initconfig()
        Poloniexapi.request()
        printscreen()
        check()

    except TypeError:
        helper.telegramsend.send("TypeError CRASHED!!!!")
        print("TypeError CRASHED!!!!")
    except AttributeError:
        helper.telegramsend.send("AttributeError CRASHED!!!!")
        print("AttributeError CRASHED!!!!")
    except Exception as e:
        helper.telegramsend.send("Error '{0}' occurred. Arguments {1}.".format(e, e.args))
        print("Error '{0}' occurred. Arguments {1}.".format(e, e.args))
    finally:
        time.sleep(10)